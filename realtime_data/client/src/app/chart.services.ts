import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import { Injectable } from '@angular/core';

export class ChartService {
  private url = 'http://angular-server-oertle-test-kafka-consumer.192.168.99.100.nip.io';
  private socket;

  getLiveData1() {
    const observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('kafkaTopic1', (data) => {
        observer.next(data);
      });
    });
    return observable;
  }
  getLiveData2() {
    const observable = new Observable(observer => {
      this.socket = io(this.url);
      this.socket.on('kafkaTopic2', (data) => {
        observer.next(data);
      });
    });
    return observable;
  }
}
