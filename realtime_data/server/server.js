'use strict';
let app = require('express')();
let http = require('http').Server(app);
let io = require('socket.io')(http);
var Kafka = require('no-kafka');


io.on('connection', (socket) => {
    console.log('USER CONNECTED');
    socket.on('disconnect', function () {
        console.log('USER DISCONNECTED');
    });
});

http.listen(8091, () => {
    console.log('started on port 8091');
    var consumer = new Kafka.SimpleConsumer({
        connectionString: 'my-cluster-kafka:9092',
        clientId: 'handsOnKafkaClient'
    });
    var dataHandler = function (messageSet, topic, partition) {
        messageSet.forEach(function (m) {
            console.log(topic, partition, m.offset, m.message.value.toString('utf8'));
            if (topic == "kafkaTopic1") {
                io.emit('kafkaTopic1', { x: (new Date()).getTime(), y: m.message.value.toString('utf8') });
            } 
            else {
                io.emit('ovp-baskets', { x: (new Date()).getTime(), y: m.message.value.toString('utf8') });
            }
        });
    };
    return consumer.init().then(function () {
        console.log("consumer initialized.....ok");
        var v1 = consumer.subscribe('kafkaTopic1', 0, dataHandler);
        var v2 = consumer.subscribe('ovp-baskets', 0, dataHandler);
		
    });
});